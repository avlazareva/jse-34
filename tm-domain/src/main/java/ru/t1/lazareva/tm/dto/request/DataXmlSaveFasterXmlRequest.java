package ru.t1.lazareva.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataXmlSaveFasterXmlRequest extends AbstractUserRequest {

    public DataXmlSaveFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}
