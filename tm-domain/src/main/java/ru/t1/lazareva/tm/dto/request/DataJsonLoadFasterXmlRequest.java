package ru.t1.lazareva.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataJsonLoadFasterXmlRequest extends AbstractUserRequest {

    public DataJsonLoadFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}
