package ru.t1.lazareva.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataBase64SaveRequest extends AbstractUserRequest {

    public DataBase64SaveRequest(@Nullable final String token) {
        super(token);
    }

}
