package ru.t1.lazareva.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataJsonSaveJaxBRequest extends AbstractUserRequest {

    public DataJsonSaveJaxBRequest(@Nullable final String token) {
        super(token);
    }

}
